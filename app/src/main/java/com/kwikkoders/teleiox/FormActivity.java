package com.kwikkoders.teleiox;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.kwikkoders.teleiox.Print.Utility.PrintReceipt;
import com.kwikkoders.teleiox.Print.Utility.StaticValue;
import com.kwikkoders.teleiox.Print.activity.DeviceListActivity;
import com.kwikkoders.teleiox.Print.preferencedata.AppPreference;
import com.kwikkoders.teleiox.Print.preferencedata.PrefKey;
import com.mocoo.hang.rtprinter.driver.Contants;
import com.mocoo.hang.rtprinter.driver.HsBluetoothPrintDriver;

import java.lang.ref.WeakReference;
import java.util.Random;

public class FormActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private EditText et_userName;
    private EditText et_phoneModel;
    private Button btn_print;
    private Context mContext;
    private Activity mActivity;
    //printer work
    private static BluetoothDevice device;
    private AlertDialog.Builder alertDlgBuilder;
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private BluetoothAdapter mBluetoothAdapter = null;
    public static HsBluetoothPrintDriver BLUETOOTH_PRINTER= null;
    Intent serverIntent = null;
    private static Context CONTEXT;
    String address;
    private static ImageView ivPrinter;
    private static ProgressDialog dialog;
    //end of printer
    private String pName, pModel;
    private  ImageView iv_back;
    int id_no=0000;

    private Spinner sp_no_of_devices,sp_phone_brand;
    private LinearLayout ll_phone_info;
    //private static OutputStream outputStream;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        initView();
        initVariable();
        initFunctionality();
        initListener();

    }

    private void initListener() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // finish();
                onBackPressed();
            }
        });
    }

    public void onStart() {
        // If BT is not on, request that to be enabled.
        // initializeBluetoothDevice() will then be called during onActivityResult
        super.onStart();

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (BLUETOOTH_PRINTER == null) {
                initializeBluetoothDevice();
            } else {
                if (BLUETOOTH_PRINTER.IsNoConnection()) {
                    // Toast.makeText(mContext,"Printer is not connected", Toast.LENGTH_SHORT).show();

                } else {
                    ivPrinter.setVisibility(View.VISIBLE);
                }
            }

        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        mContext.registerReceiver(mReceiver, filter);

    }

        private void initFunctionality() {

        //Bluetooth Printer//
        //CONTEXT = getApplicationContext();
        alertDlgBuilder = new AlertDialog.Builder(mContext);

        // Get device's Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not available in your device
        if (mBluetoothAdapter == null) {
            Toast.makeText(mContext, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            if (BLUETOOTH_PRINTER != null) {
                BLUETOOTH_PRINTER.stop();
            }
            finish();

        }

            ArrayAdapter<CharSequence> noOfDevice = ArrayAdapter.createFromResource(this,
                    R.array.no_of_devices, R.layout.simple_spinner_item);
            noOfDevice.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            sp_no_of_devices.setAdapter(noOfDevice);
            // Setting OnItemClickListener to the Spinner
            sp_no_of_devices.setOnItemSelectedListener(this);

            ArrayAdapter<CharSequence> phoneBrand = ArrayAdapter.createFromResource(this,
                    R.array.phone_brand, R.layout.simple_spinner_item);
            phoneBrand.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            sp_phone_brand.setAdapter(phoneBrand);
            // Setting OnItemClickListener to the Spinner
            sp_phone_brand.setOnItemSelectedListener(this);


        }

    @Override
    protected void onResume() {
        super.onResume();


    //Print button //
        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(TextUtils.isEmpty(et_phoneModel.getText().toString()) || TextUtils.isEmpty(et_userName.getText().toString()))
                {
                    Toast.makeText(getApplicationContext(), "Please fill up the form!",
                            Toast.LENGTH_LONG).show();
                }
                else {

                    pName = et_userName.getText().toString();
                    pModel = et_phoneModel.getText().toString();


                    //Log.d("TicketNo",id_no+"");

                    bluetoothConnection();
                    //Random random= new Random();
                    //@SuppressLint("DefaultLocale")
                   // String id = String.format("%04d",1);
                    // PrintReceipt.printBillFromOrder(mContext,pName,pModel);
                }
            }
        });

    }

    private void bluetoothConnection() {

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
        else {
            //If the connection is lost with last connected bluetooth printer
            if (BLUETOOTH_PRINTER == null){
                initializeBluetoothDevice();
            }
            if (BLUETOOTH_PRINTER.IsNoConnection()) {
                if (AppPreference.getInstance(mActivity).getString(PrefKey.PRINTER_NAME).equals("")){
                    serverIntent = new Intent(mActivity, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                }
                else{
                    dialog = ProgressDialog.show(FormActivity.this,"Connecting...","Please Wait!",true);
                    String address = AppPreference.getInstance(mActivity.getApplicationContext()).getString(PrefKey.PRINTER_NAME);
                    device = mBluetoothAdapter.getRemoteDevice(address);
                    BLUETOOTH_PRINTER.start();
                    BLUETOOTH_PRINTER.connect(device);
                }
            } else {

                int  id_no = Integer.parseInt(AppPreference.getInstance(mContext).getTicketNo(PrefKey.ID_NO))+1;
                Log.d("TicketNo",id_no+"");
                PrintReceipt.printBillFromOrder(CONTEXT, pName,pModel);
                AppPreference.getInstance(mContext).setTicketNo(PrefKey.ID_NO,id_no+"");


            }
        }

    }
    private void initializeBluetoothDevice() {

        // Initialize HsBluetoothPrintDriver class to perform bluetooth connections
        BLUETOOTH_PRINTER = HsBluetoothPrintDriver.getInstance();//
        BLUETOOTH_PRINTER.setHandler(new BluetoothHandler(FormActivity.this));
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


        /*if (sp_no_of_devices.getSelectedItemPosition()==1){
            for (int j=0;j<1;j++){
                ll_phone_info.setVisibility(View.VISIBLE);
            }
        }else if (sp_no_of_devices.getSelectedItemPosition()==2){
            for (int j=0;j<2;j++){
                ll_phone_info.setVisibility(View.VISIBLE);
            }
        }else if (sp_no_of_devices.getSelectedItemPosition()==3){
            for (int j=0;j<3;j++){
                ll_phone_info.setVisibility(View.VISIBLE);
            }
        }else if (sp_no_of_devices.getSelectedItemPosition()==4){
            for (int j=0;j<4;j++){
                ll_phone_info.setVisibility(View.VISIBLE);
            }
        }else if (sp_no_of_devices.getSelectedItemPosition()==5) {
            for (int j = 0; j < 5; j++) {
                ll_phone_info.setVisibility(View.VISIBLE);
            }
        }
        else{
            ll_phone_info.setVisibility(View.VISIBLE);
            }
*/
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private static class BluetoothHandler extends Handler {
        private final WeakReference<FormActivity> myWeakReference;

        //Creating weak reference of BluetoothPrinterActivity class to avoid any leak
        BluetoothHandler(FormActivity weakReference) {
            myWeakReference = new WeakReference<FormActivity>(weakReference);
        }

        @Override
        public void handleMessage(Message msg) {
            Activity formActivity = myWeakReference.get();
            if (formActivity != null) {
                super.handleMessage(msg);
                Bundle data = msg.getData();
                switch (data.getInt("flag")) {
                    case Contants.FLAG_STATE_CHANGE:
                        int state = data.getInt("state");
                        Log.i("", "MESSAGE_STATE_CHANGE: " + state);

                        switch (state) {
                            case HsBluetoothPrintDriver.CONNECTED_BY_BLUETOOTH:

                                StaticValue.isPrinterConnected = true;
                                if (dialog !=  null) {
                                    dialog.dismiss();
                                }
                                Toast.makeText(CONTEXT, "Connection successful", Toast.LENGTH_SHORT).show();
                                ivPrinter.setVisibility(View.VISIBLE);
                                Toast.makeText(CONTEXT, "Click Print Again,Please...", Toast.LENGTH_SHORT).show();


                                break;
                            case HsBluetoothPrintDriver.FLAG_SUCCESS_CONNECT:
                                //ivPrinter.setVisibility(View.VISIBLE);
                                //tvPrinterStatus.setText(R.string.title_connecting);
                                //Toast.makeText(CONTEXT,"connecting",Toast.LENGTH_SHORT).show();
                                break;

                            case HsBluetoothPrintDriver.UNCONNECTED:
                                // tvPrinterStatus.setText(R.string.no_printer_connected);
                                Toast.makeText(CONTEXT,"Printer Disconnected",Toast.LENGTH_SHORT).show();
                                ivPrinter.setVisibility(View.GONE);

                                break;
                        }
                        break;
                    case Contants.FLAG_SUCCESS_CONNECT:
                        // tvPrinterStatus.setText(R.string.title_connecting);
                        //Toast.makeText(CONTEXT,"connecting",Toast.LENGTH_SHORT).show();
                        break;
                    case Contants.FLAG_FAIL_CONNECT:
                        try {
                            if (dialog != null) {
                                dialog.dismiss();
                                Log.d("DeviceStatus", "Showing dialog");
                            }
                            Toast.makeText(CONTEXT, "Connection failed.", Toast.LENGTH_SHORT).show();
                            //dialog.dismiss();
                            ivPrinter.setVisibility(View.GONE);

                        }catch (Exception e){
                            if (dialog != null) {
                                dialog.dismiss();
                                Log.d("DeviceStatus", "Showing dialog");
                            }
                            Toast.makeText(CONTEXT, "Connection failed.Please turn on your printer", Toast.LENGTH_SHORT).show();

                        }
                        break;
                    default:
                        break;

                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("", "onActivityResult " + requestCode);
        switch (requestCode) {

            case REQUEST_CONNECT_DEVICE:
                Log.d("", "onActivityResult1 " + requestCode);

                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    //dialog = ProgressDialog.show(CONTEXT,"Connecting...","Please Wait!",true);
                    // Get the device MAC address
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    device = mBluetoothAdapter.getRemoteDevice(address);
                    Log.d("macaddress", device + "");

                    AppPreference.getInstance(mActivity.getApplicationContext()).setString(PrefKey.PRINTER_NAME, address);
                    // Attempt to connect to the device
                    BLUETOOTH_PRINTER.start();
                    BLUETOOTH_PRINTER.connect(device);
                    //BLUETOOTH_PRINTER.connect(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    initializeBluetoothDevice();

                } else {
                    // User did not enable Bluetooth or an error occured
                    //   Log.d(TAG, "BT not enabled");
                    Toast.makeText(CONTEXT, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    //mActivity().finish();
                }
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (BLUETOOTH_PRINTER != null /*&& BLUETOOTH_PRINTER.IsNoConnection()*/) {
            BLUETOOTH_PRINTER.stop();
        }
        mContext.unregisterReceiver(mReceiver);
    }

    //The BroadcastReceiver that listens for bluetooth broadcasts
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //Device found
                Log.e("DeviceStatus", "Device found");
            }
            else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                //Device is now connected
                Log.e("DeviceStatus", "Device is now connected");
                ivPrinter.setVisibility(View.VISIBLE);
                if(dialog != null){
                    dialog.dismiss();
                }
            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //Done searching
                Log.e("DeviceStatus", "Done searching");
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
                //Device is about to disconnect
                Log.e("DeviceStatus", "Device is about to disconnect");
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                //Device has disconnected
                Log.e("DeviceStatus", "Device has disconnected");
                //dialog.dismiss();
                if(dialog != null){
                    dialog.dismiss();
                }
                BLUETOOTH_PRINTER.stop();
                Toast.makeText(CONTEXT,"Device has disconnected",Toast.LENGTH_SHORT).show();
                ivPrinter.setVisibility(View.GONE);
            }
        }
    };



    private void initVariable() {
        mActivity= FormActivity.this;
        mContext= getApplicationContext();
        CONTEXT= getApplicationContext();

    }

    private void initView() {

        et_userName = findViewById(R.id.et_user_name);
        et_phoneModel = findViewById(R.id.et_phone_model);
        btn_print = findViewById(R.id.btn_print);
        ivPrinter = findViewById(R.id.iv_printerReport);
        iv_back= findViewById(R.id.iv_back);
        sp_no_of_devices = findViewById(R.id.sp_number_of_device);
        sp_phone_brand = findViewById(R.id.sp_phone_brand);
        ll_phone_info= findViewById(R.id.ll_phone_info);

    }

}
