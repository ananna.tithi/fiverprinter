package com.kwikkoders.teleiox;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.kwikkoders.teleiox.Print.preferencedata.AppPreference;
import com.kwikkoders.teleiox.Print.preferencedata.PrefKey;

public class MainActivity extends AppCompatActivity {

    private ImageView ivLogo;
    private Button btn_generate_receipt;
    private Context mContext;
    private Activity mActivity;
    private Intent intent;
    String id= "1000";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initView();
        initVariable();
        initListener();
        if(AppPreference.getInstance(mContext).getTicketNo(PrefKey.ID_NO).isEmpty())
            AppPreference.getInstance(mContext).setTicketNo(PrefKey.ID_NO,id);


        //AppPreference.getInstance(getApplicationContext()).setString(PrefKey.PRINTER_NAME,null);


        //SSCalendar.generateUniqueNum();
    }

    private void initListener() {

        btn_generate_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intent = new Intent(mActivity,FormActivity.class);
                startActivity(intent);

            }
        });
    }

    private void initVariable() {
        mActivity= MainActivity.this;
        mContext= getApplicationContext();

    }

    private void initView() {

        ivLogo = findViewById(R.id.iv_logo);
        btn_generate_receipt = findViewById(R.id.btn_receipt);

    }

    @Override
    public void onBackPressed() {

       // AppUtility.exitApp(mActivity);
       // super.onBackPressed();

        AlertDialog.Builder alertdialog=new AlertDialog.Builder(this);
        alertdialog.setTitle(R.string.app_name);
        alertdialog.setIcon(R.drawable.logo);
        alertdialog.setMessage("Do you want to exit?");
        alertdialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AppPreference.getInstance(mActivity).setString(PrefKey.PRINTER_NAME,"");
                mActivity.finish();
            }
        });

        alertdialog.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert=alertdialog.create();
        alertdialog.show();


    }
}
