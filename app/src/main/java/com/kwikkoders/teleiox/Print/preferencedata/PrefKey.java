package com.kwikkoders.teleiox.Print.preferencedata;

public class PrefKey {

    // preference keys
    public static final String APP_PREFERENCE  = "app_prefs";

    // common constant
    public static final String OPENED = "opened";

    // User info/ billing
    public static final String PRINTER_NAME = "printer_name";
    public static final String  ID_NO = "id_no";

}
