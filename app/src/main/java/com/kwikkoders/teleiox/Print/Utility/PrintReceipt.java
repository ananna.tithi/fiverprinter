package com.kwikkoders.teleiox.Print.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.kwikkoders.teleiox.FormActivity;
import com.kwikkoders.teleiox.Print.preferencedata.AppPreference;
import com.kwikkoders.teleiox.Print.preferencedata.PrefKey;
import com.kwikkoders.teleiox.R;
import com.kwikkoders.teleiox.Utils;

import java.io.IOException;
import java.io.OutputStream;
import java.text.Normalizer;


/**
 * This class is responsible to generate a static sales receipt and to print that receipt
 */
public class PrintReceipt {

	private static OutputStream outputStream;
	private static Context mContext;

	public static boolean printBillFromOrder(Context context, String pName, String pModel){

		mContext = context;

		if(FormActivity.BLUETOOTH_PRINTER.IsNoConnection()){
			return false;
		}

		//route information get into the database


		byte[] cc = new byte[]{0x1B,0x6,0x00};  // 0- normal size text
		byte[] bb = new byte[]{0x1B,0x21,0x23};  // 1- only bold text
		byte[] bb2 = new byte[]{0x1B,0x21,0x20}; // 2- bold with medium text
		byte[] bb3 = new byte[]{0x1B,0x21,0x10}; // 3- bold with large text
		byte[] bb4 = new byte[]{0x1B,0x21,0x18}; // 4- bold with large medium text

		double totalBill=0.00, netBill=0.00, totalDiscount=0.00;

		FormActivity.BLUETOOTH_PRINTER.Begin();
		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();

		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//CENTER
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal
		FormActivity.BLUETOOTH_PRINTER.BT_Write(bb3);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("Customer's Receipt"+"\n\n");


		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//LEFT
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal
		FormActivity.BLUETOOTH_PRINTER.BT_Write(printPhoto(R.drawable.printable_logo));
		//FormActivity.BLUETOOTH_PRINTER.BT_Write("\nMarket Code:"+marketCode+"");

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 0);//CENTER
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.BT_Write(cc);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("\n");
		FormActivity.BLUETOOTH_PRINTER.LF();

		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 0);//LEFT
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 25);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal
		FormActivity.BLUETOOTH_PRINTER.BT_Write(bb2);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("ID No:T-ASO-"+AppPreference.getInstance(mContext).getTicketNo(PrefKey.ID_NO)+"");
		FormActivity.BLUETOOTH_PRINTER.BT_Write("\n");


		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//center
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 25);
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);
		//BT_Write() method will initiate the printer to start printing.
		FormActivity.BLUETOOTH_PRINTER.BT_Write("Date&Time: " + SSCalendar.getCurrentDate()+ " ");
		FormActivity.BLUETOOTH_PRINTER.BT_Write(" " +SSCalendar.getCurrentTime()+"\n");

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 0);//LEFT
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);	//50 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.BT_Write(cc);

			//FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal


			//totalBill=totalBill + (salesModel.getUnitSalesCost() * salesModel.getSalesAmount());
			//totalBill= (salesModel.getUnitSalesCost() * salesModel.getSalesAmount());
			//Log.d("getDiscount",totalDiscount+"");

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.BT_Write(cc);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("\n" +"Thank you for using our service"+ "\n");
		//FormActivity.BLUETOOTH_PRINTER.BT_Write(bb3);
		FormActivity.BLUETOOTH_PRINTER.LF();


		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//CENTER
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal
		FormActivity.BLUETOOTH_PRINTER.BT_Write(bb2);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("@ TELEIOX"+"\n\n");


		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 0);//LEFT
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 25);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal
		FormActivity.BLUETOOTH_PRINTER.BT_Write(printPhoto(R.drawable.cut));
		FormActivity.BLUETOOTH_PRINTER.BT_Write(context.getResources().getString(R.string.print_line));
		FormActivity.BLUETOOTH_PRINTER.BT_Write("\n");

		//totalVat=Double.parseDouble(Utility.doubleFormatter(totalBill*(StaticValue.VAT/100)));
		//netBill=totalBill-totalDiscount;

//		FormActivity.BLUETOOTH_PRINTER.LF();
//		FormActivity.BLUETOOTH_PRINTER.BT_Write("Total Bill:              "+ Utility.doubleFormatter(totalBill) + ""+"\n");
//		FormActivity.BLUETOOTH_PRINTER.LF();


//		FormActivity.BLUETOOTH_PRINTER.LF();
//		FormActivity.BLUETOOTH_PRINTER.BT_Write( "Discount:                    " +(totalDiscount)+ ""+"\n");

//		FormActivity.BLUETOOTH_PRINTER.LF();
//		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//center
//		FormActivity.BLUETOOTH_PRINTER.BT_Write(context.getResources().getString(R.string.print_line)+"\n");


//		FormActivity.BLUETOOTH_PRINTER.LF();
//		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);
//		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 2);//Right
//		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x8);
		//FormActivity.BLUETOOTH_PRINTER.BT_Write("Net Bill:" + Utility.doubleFormatter(netBill) +"");

//		FormActivity.BLUETOOTH_PRINTER.LF();
//		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//center
//		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal font
//		//FormActivity.BLUETOOTH_PRINTER.BT_Write(context.getResources().getString(R.string.print_line));
		//FormActivity.BLUETOOTH_PRINTER.BT_Write("\n\n\n\n");

		//Office Receipt

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();
//
		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//CENTER
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal
		FormActivity.BLUETOOTH_PRINTER.BT_Write(bb3);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("Office Receipt"+"\n");

		FormActivity.BLUETOOTH_PRINTER.Begin();
		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();

		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//CENTER
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal
		FormActivity.BLUETOOTH_PRINTER.BT_Write((printPhoto(R.drawable.printable_logo)));

		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 0);//LEFT
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 30);	//50 * 0.125mm
		//FormActivity.BLUETOOTH_PRINTER.BT_Write(cc);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("\n\n");
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal font

		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 0);//LEFT
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 25);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal
		FormActivity.BLUETOOTH_PRINTER.BT_Write(bb2);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("ID No:T-ASO-"+AppPreference.getInstance(mContext).getTicketNo(PrefKey.ID_NO)+"");
		FormActivity.BLUETOOTH_PRINTER.BT_Write("\n");

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 1);//CENTER
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 25);
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);
		FormActivity.BLUETOOTH_PRINTER.BT_Write(bb);
		FormActivity.BLUETOOTH_PRINTER.BT_Write(SSCalendar.getCurrentDate()+ "  ");
		FormActivity.BLUETOOTH_PRINTER.BT_Write(SSCalendar.getCurrentTime()+ "\n");

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();

		//static sales record are generated
		//Log.d("getDiscount",totalDiscount+"");

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 0);//LEFT
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 25);
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);
		FormActivity.BLUETOOTH_PRINTER.BT_Write(cc);
		FormActivity.BLUETOOTH_PRINTER.BT_Write("  "+pName+"\n");
		FormActivity.BLUETOOTH_PRINTER.BT_Write("  "+pModel+"\n\n");

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.LF();

		FormActivity.BLUETOOTH_PRINTER.LF();
		FormActivity.BLUETOOTH_PRINTER.SetAlignMode((byte) 0);//left
		FormActivity.BLUETOOTH_PRINTER.SetLineSpacing((byte) 25);	//30 * 0.125mm
		FormActivity.BLUETOOTH_PRINTER.SetFontEnlarge((byte) 0x00);//normal font
		FormActivity.BLUETOOTH_PRINTER.BT_Write(context.getResources().getString(R.string.print_line));
		FormActivity.BLUETOOTH_PRINTER.BT_Write(" \n");

		return true;
	}

	//print photo
	public static byte[] printPhoto(int img) {
		try {
			Bitmap bmp = BitmapFactory.decodeResource(mContext.getResources(),
					img);
			if(bmp!=null){
				byte[] command = Utils.decodeBitmap(bmp);
				//outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
				//printText(command);
				return command;
			}else{
				Log.e("Print Photo error", "the file isn't exists");
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("PrintTools", "the file isn't exists");
			return null;
		}
	}

	//print byte[]
	private void printText(byte[] msg) {
		try {
			// Print normal text
			outputStream.write(msg);
			//printNewLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
